import Footer from '@/components/footer';
import Header from '@/components/header';
import { Check } from 'lucide-react';
import React from 'react';

const AboutUs = () => {
  return (
        <>
            <Header />
            <div className='w-[1000px] mx-auto py-20'>
                <h3 className='py-10 font-semibold text-3xl text-primary_green'>Bienvenido a AgroSmart</h3>
                <p className='text-lg '>En AgroSmart, estamos comprometidos con ofrecerte la frescura y calidad de los productos agrícolas directamente a tu puerta. Nos enorgullece ser tu destino número uno para frutas y verduras frescas, cultivadas con cuidado y pasión por agricultores locales.</p>
                <h3 className='py-10 font-semibold text-3xl text-primary_green'>Nuestra Misión</h3>
                <p>Nuestra misión en AgroSmart es revolucionar la forma en que las personas acceden a productos frescos. Creemos en la importancia de una alimentación saludable y sostenible, y trabajamos arduamente para hacer que los alimentos frescos y nutritivos sean accesibles para todos. Además, estamos comprometidos con apoyar a los agricultores locales, promoviendo prácticas agrícolas responsables y contribuyendo al bienestar de nuestras comunidades.</p>
                <h3 className='py-10 font-semibold text-3xl text-primary_green'>Nuestro Compromiso</h3>
                <p>En AgroSmart, la calidad y la frescura son nuestra máxima prioridad. Trabajamos en estrecha colaboración con agricultores locales que comparten nuestros valores de integridad, sostenibilidad y excelencia. Nos comprometemos a seleccionar cuidadosamente cada producto para garantizar su frescura y calidad, y a brindarte una experiencia de compra en línea conveniente y satisfactoria.</p>
                <h3 className='py-10 font-semibold text-3xl text-primary_green'>¿Por Qué Elegir AgroSmart?</h3>
                <ul>
                    <li className='py-4'>
                         <div className='flex items-center gap-2'>
                            <h4 className='text-lg font-semibold'>Frescura Garantizada </h4>
                            <Check />
                        </div>
                        <p>Nos asociamos con agricultores locales para ofrecerte productos frescos y de alta calidad directamente desde el campo.</p>
                    </li>
                    <li className='py-4'>
                        <div className='flex items-center gap-2'>
                            <h4 className='text-lg font-semibold'>Variedad y Diversidad:</h4>
                            <Check />
                        </div>
                        <p>Desde las frutas y verduras más populares hasta las variedades más exóticas, en AgroSmart encontrarás una amplia gama de productos frescos para satisfacer tus necesidades.</p>
                    </li>
                    <li className='py-4'>
                        <div className='flex items-center gap-2'>
                            <h4 className='text-lg font-semibold'>Comunidad Local: </h4>
                            <Check />
                        </div>
                        <p>Al elegir AgroSmart, estás apoyando a agricultores locales y contribuyendo al crecimiento económico y social de tu comunidad.</p>
                    </li>
                    <li className='py-4'>
                        <div className='flex items-center gap-2'>
                            <h4 className='text-lg font-semibold'>Comodidad:  </h4>
                            <Check />
                        </div>
                        <p>Nuestro servicio de entrega a domicilio te permite disfrutar de productos frescos sin tener que salir de casa, ahorrándote tiempo y esfuerzo.</p>
                    </li>
                </ul>
            </div>
            <Footer />
        </>
  );
};

export default AboutUs;
