import SidebarDashboard from '@/app/components/sidebar-dashboard';
import { ReactNode } from 'react';

export default function Layout ({ children }: { children: ReactNode }) {
  return (
    <>
      <div className='flex'>
        <SidebarDashboard />
        <div className='flex-1'>
          {children}
        </div>
      </div>
    </>
  );
}
