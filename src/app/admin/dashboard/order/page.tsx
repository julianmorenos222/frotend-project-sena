import TableOrders from '@/app/components/table-orders';
import React from 'react';

const page = () => {
  return (
    <div className='pt-10 w-full px-20 mx-auto'>
        <TableOrders/>
    </div>
  );
};

export default page;
