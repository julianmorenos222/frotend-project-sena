import FormCreateProduct from '@/app/components/form-create-product';

const page = () => {
  return (
        <div className='flex justify-center gap-16 pt-20 w-full px-10 mx-auto'>
            <FormCreateProduct/>
        </div>
  );
};

export default page;
