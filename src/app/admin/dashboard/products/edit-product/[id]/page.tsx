import FormEditProduct from '@/app/components/form-edit-product';
import { getOneProduct } from '@/services/product/prouduct.service';
import React from 'react';

const page = async ({ params }: { params: { id: string } }) => {
  const res = await getOneProduct(Number(params.id));
  return (
        <div className='pt-10 w-full px-20 mx-auto'>
            <div className='flex mx-auto max-w-4xl gap-4'>

                {
                    res && (
                        <>
                            <FormEditProduct product={res.raw} />
                            <div className='grow'>
{/*                                 <Image alt={'image'} className='w-full' width={1000} height={1000} src={res.raw.imgUrl} />
 */}                            </div>
                        </>
                    )
                }
            </div>

        </div>
  );
};

export default page;
