import TableProducts from '@/app/components/table-products';
import { Button } from '@/components/ui/button';
import Link from 'next/link';

export default function Products () {
  return (
    <div className='pt-10 w-full px-20 mx-auto'>
      <Link href='/admin/dashboard/products/create-product' >
        <Button >Create Product</Button>
      </Link>
      <TableProducts />

    </div>

  );
}
