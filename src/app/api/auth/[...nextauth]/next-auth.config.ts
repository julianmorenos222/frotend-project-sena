import { NextAuthOptions, Session } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

export const authOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      credentials: {
        username: { label: 'Username', type: 'text' },
        password: { label: 'Password', type: 'password' }
      },
      async authorize (credentials, req) {
        const res = await fetch(
          `${process.env.NEXT_PUBLIC_BACKEND_URL}auth/signin`,
          {
            method: 'POST',
            body: JSON.stringify({
              username: credentials?.username,
              password: credentials?.password
            }),
            headers: { 'Content-Type': 'application/json' }
          }
        );
        const user = await res.json();
        if (user.errorCode) throw user;

        return user;
      }
    })
  ],
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    async jwt ({ token, user, trigger, session }: any) {
      if (trigger === 'update') {
        return { ...token, ...session.user.raw };
      }
      return { ...token, ...user };
    },
    async session ({ session, token }: { session: Session; token: any }) {
      session.user = token as any;
      return session;
    }
  },
  pages: {
    signIn: '/admin/auth/signin'
  }
};
