import Footer from '@/components/footer';
import Header from '@/components/header';
import React from 'react';
import FormCheckout from '../components/form-checkout/form-checkout';

const page = () => {
  return (
    <>
      <Header />
      <main className='py-20' >
        <FormCheckout />
      </main>
      <Footer />
    </>
  );
};

export default page;
