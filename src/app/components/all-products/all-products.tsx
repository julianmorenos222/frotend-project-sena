import CardProduct from '../card-product';
import { Button } from '@/components/ui/button';
import Link from 'next/link';
import { getAllProducts } from '@/services/product/prouduct.service';
import { ProductItemType } from '@/types/product/product';

const getProducts = async () => {
  const res = await getAllProducts();
  return res.raw;
};

const AllProducts = async () => {
  const data = await getProducts();
  return (
    <div className='max-w-[1400px] mx-auto'>
      <h2 className='py-10 font-semibold text-3xl text-secondary'>Ultimos productos</h2>
      <div className='flex w-full flex-wrap  gap-10 py-8'>
        {
          data && data.sort((a: ProductItemType, b: ProductItemType) => new Date(b.date).getTime() - new Date(a.date).getTime()).slice(0, 4).map((product) => (
            <CardProduct key={product.id} product={product} />
          ))
        }
      </div>
      <Link href={'/product/category/all'}><Button className='relative left-1/2 -translate-x-1/2'> Ver todos</Button></Link>
    </div>
  );
};

export default AllProducts;
