import Image from 'next/image';
import React from 'react';

const CardFeaturedProduct = () => {
  return (
    <div className='flex h-72 w-96'>
      <div>
        <Image alt={'image'} className='w-full h-full' width={500} height={500} src={'/assets/img/exampleCardFeatured.png'}/>
      </div>
      <div className='flex flex-col justify-center h-full px-8'>
          <h3 className='text-lg'>Line Towel</h3>
          <p className='text-lg font-medium'>30$</p>
      </div>
    </div>
  );
};

export default CardFeaturedProduct;
