'use client';
import { Button } from '@/components/ui/button';
import { ShoppingCartContext, ShoppingCartType } from '@/context/products/shopping-cart';
import { ProductItemType } from '@/types/product/product';
import { convertToPrice } from '@/utils/converterToPrice';
import Image from 'next/image';
import Link from 'next/link';
import React, { useContext } from 'react';

type Props = {
  product: ProductItemType
}

const CardProduct: React.FC<Props> = ({ product: { id, name, price, imgUrl, amountStock, description, idCategory, date } }) => {
  const { addItem } = useContext<ShoppingCartType>(ShoppingCartContext);

  return (
    <div className='w-80 h-[524px] group bg-[#F8F8F8] shadow-md rounded-t-xl overflow-hidden'>
      <div className='relative flex items-center overflow-hidden w-full bg-orange/35 h-96'>
          <Image alt={'image'} className='w-full' width={800} height={500} src={imgUrl} />

        <div className='flex  justify-center items-center -top-24 absolute left-0 bg-black/40 w-full h-0 group-hover:top-0 group-hover:h-full duration-500 transition-allgroup-hover:bg-black/50'>
          <div className='flex flex-col gap-4'>
            <Link href={`/product/${id}`}>
              <Button className=' w-full' >
                Ver
              </Button>
            </Link>
            <Button onClick={() => addItem({ id, name, price, imgUrl, amountStock, description, idCategory, date })} variant={'outline'} className='border-white text-white'>Añadir al carrito</Button>
          </div>

        </div>
      </div>
      <div className='py-8 px-4'>
        <h3 className='text-xl'>{name}</h3>
        <p className='text-lg font-medium text-red-500'>{convertToPrice(Number(price))}</p>
      </div>
    </div>
  );
};

export default CardProduct;
