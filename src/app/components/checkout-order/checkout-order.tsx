'use client';
import Spinner from '@/components/spinner';
import { Button } from '@/components/ui/button';
import { ShoppingCartContext, ShoppingCartType } from '@/context/products/shopping-cart';
import { convertToPrice } from '@/utils/converterToPrice';
import React, { useContext } from 'react';

type Props = {
    loading: boolean;
};

const CheckoutOrder: React.FC<Props> = ({ loading }) => {
  const { items, amountPriceTotal } = useContext<ShoppingCartType>(ShoppingCartContext);

  return (
        <div className='border h-full py-6 px-8 w-full'>
            <h3 className=' text-lg font-semibold'>Tu orden</h3>
            <div className='flex h-full flex-col'>
                <section className='flex py-6 justify-between border-b'>
                    <p>Productos</p>
                    <p>Subtotal</p>
                </section>
                <section className='flex-1'>
                    {
                        items.length > 0 && items.map((item) => (
                            <div className='flex py-6 justify-between' key={item.id}>
                                <p>{item.name}</p>
                                <p>X {item.amount}</p>
                                <p>{convertToPrice(Number(item.price))}</p>
                            </div>
                        ))
                    }
                </section>
                <section className='flex-1'>
                    <div className='flex py-6 justify-between border-b'>
                        <p >Total:</p>
                        <p>{amountPriceTotal}</p>
                    </div>
                    <p className='my-6 bg-slate-100 p-4 text-gray-600'>Pago a contraentrega</p>
                    <Button className='w-full'>
                        {loading ? <Spinner/> : 'Finalizar pedido'}
                    </Button>
                </section>
            </div>
        </div>
  );
};

export default CheckoutOrder;
