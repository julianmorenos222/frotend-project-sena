'use client';
import { Button } from '@/components/ui/button';
import React, { useContext, useEffect, useState } from 'react';
import Image from 'next/image';
import { ProductItemType } from '@/types/product/product';
import { ShoppingCartContext, ShoppingCartType } from '@/context/products/shopping-cart';
import { getAllProductsByCategory } from '@/services/product/prouduct.service';
import CardProduct from '../card-product';
import { convertToPrice } from '@/utils/converterToPrice';

type Props = {
    product: ProductItemType
}

const DetailProduct: React.FC<Props> = ({ product: { id, name, price, imgUrl, amountStock, description, idCategory, date } }) => {
  const [productsRelated, setProductsRelated] = useState<ProductItemType[]>([]);
  const { addItem } = useContext<ShoppingCartType>(ShoppingCartContext);

  useEffect(() => {
    const fetchProducts = async () => {
      const res = await getAllProductsByCategory(idCategory);
      setProductsRelated(res.raw);
    };
    fetchProducts();
  }, [idCategory]);
  return (
        <div className='w-full max-w-[1400px] mx-auto py-20'>
            <div className='w-full flex gap-10 flex-wrap'>
                <div className='w-[50%] flex gap-4 '>
                    <figure className='flex-1 '>
                        <Image alt={'image'} className='w-full mx-auto max-w-[600px]' width={1000} height={1000} src={imgUrl} />
                    </figure>
                </div>
                <div className='flex w-[40%] flex-col gap-4 '>
                    <h3 className='text-3xl font-semibold'>{name}</h3>
                    <p className='text-2xl text-red-500 font-medium'>{convertToPrice(Number(price))}</p>
                <p className='text-base text-gray-500'>{ description }</p>
               <Image alt={'image'} className='' width={100} height={100} src={imgUrl} />
                    <small className=' text-gray-700'> Amount in stock: {amountStock}</small>
                    <Button onClick={() => addItem({ id, name, price, imgUrl, amountStock, description, idCategory, date })}>Add to cart</Button>
                </div>
                <hr className='w-full' />
                <div className='w-full min-h-72 mt-5 mr-10  px-14 space-y-10'>
                    <h4 className='text-3xl text-secondary text-center font-semibold'> Productos relacionados</h4>
                    <div className='w-full flex gap-10'>
                        {
                            productsRelated.slice(0, 4).filter((product) => product.id !== id).map((product) => (
                                <CardProduct key={product.id} product={product} />
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
  );
};

export default DetailProduct;
