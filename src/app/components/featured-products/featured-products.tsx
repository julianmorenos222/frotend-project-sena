import React from 'react';
import CardFeaturedProduct from '../card-featured-product';

const FeaturedProducts = () => {
  return (
    <div className='max-w-[1400px] mx-auto py-10'>
        <h2 className='py-10'>Featured Products</h2>
        <div className='flex flex-wrap w-full justify-between gap-4'>
            {
                [1, 2, 3, 4].map((product) => (
                    <CardFeaturedProduct key={product}/>
                ))
            }
        </div>
    </div>
  );
};

export default FeaturedProducts;
