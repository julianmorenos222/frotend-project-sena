'use client';
import { createOrder } from '@/services/order/order.service';
import React, { useContext, useState } from 'react';
import CheckoutOrder from '../checkout-order/checkout-order';
import { ShoppingCartContext, ShoppingCartType } from '@/context/products/shopping-cart';
import { toast } from '@/components/ui/use-toast';

const FormCheckout = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const { items } = useContext<ShoppingCartType>(ShoppingCartContext);
  const [formData, setFormData] = useState({
    name: '',
    lastName: '',
    email: '',
    phone: '',
    address: '',
    city: '',
    zipCode: '',
    description: ''
  });

  const handleFormChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };
  const detailOrder = items.map((item) => ({ idProduct: Number(item.id), amount: item.amount, unitPrice: Number(item.price) }));
  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoading(true);
    const res = await createOrder({ ...formData, detailOrder });
    if (res) {
      toast({
        title: 'Order created successfully',
        description: 'We have sent you an email with the details of your order.'
      });
    } else {
      toast({
        title: 'Error creating order',
        description: 'Please try again later.'
      });
    }
    setLoading(false);
  };
  return (
        <>
          {items.length > 0
            ? (
            <form onSubmit={handleSubmit} className='flex w-full gap-6 max-w-5xl mx-auto'>
            <div className=''>
                <div className='flex flex-col gap-8 border-b'>
                    <h1 className='text-lg font-semibold py-4'>Información</h1>
                </div>
                <section className='flex gap-4'>
                    <div className='w-1/2 flex flex-col gap-1 my-4'>
                        <label htmlFor="" className='text-sm font-semibold text-gray-500'>Nombre</label>
                        <input required value={formData.name} onChange={handleFormChange} name='name' className='border h-10' placeholder='' type="text" />
                    </div>
                    <div className='w-1/2 flex flex-col gap-1 my-4'>
                        <label htmlFor="" className='text-sm font-semibold text-gray-500'>Apellido</label>
                        <input required value={formData.lastName} onChange={handleFormChange} name='lastName' className='border h-10' placeholder='' type="text" />
                    </div>
                </section>
                <section className='flex gap-4'>
                    <div className='w-1/2 flex flex-col gap-2 my-4'>
                        <label htmlFor="" className='text-sm font-semibold text-gray-500'>Email</label>
                        <input required value={formData.email} onChange={handleFormChange} name='email' className='border h-10' placeholder='' type="text" />
                    </div>
                    <div className='w-1/2 flex flex-col gap-2 my-4'>
                        <label htmlFor="" className='text-sm font-semibold text-gray-500'>Telefono</label>
                        <input required value={formData.phone} onChange={handleFormChange} name='phone' className='border h-10' placeholder='' type="text" />
                    </div>
                </section>
                <div className='flex flex-col gap-2 my-4'>
                    <label htmlFor="" className='text-sm font-semibold text-gray-500'>Direccion</label>
                    <input required value={formData.address} onChange={handleFormChange} name='address' className='border h-10' placeholder='' type="text" />
                </div>
                <div className='flex flex-col gap-2 my-4'>
                    <label htmlFor="" className='text-sm font-semibold text-gray-500'> Codigo postal</label>
                    <input required value={formData.zipCode} onChange={handleFormChange} name='zipCode' className='border h-10' placeholder='' type="text" />
                </div>
                <div className='flex flex-col gap-2 my-4'>
                    <label htmlFor="" className='text-sm font-semibold text-gray-500'>Ciudad</label>
                    <input required value={formData.city} onChange={handleFormChange} name='city' className='border h-10' placeholder='' type="text" />
                </div>
                <div className='flex flex-col gap-2 my-4'>
                    <label htmlFor="" className='text-sm font-semibold text-gray-500'>Algo mas que debamos saber?</label>
                    <textarea value={formData.description} onChange={handleFormChange} className='border resize-none' name="description" id="" cols={30} rows={5}></textarea>
                </div>
            </div>
            <CheckoutOrder loading={loading} />

        </form>
              )
            : (
            <div className='flex justify-center'>
              <h2 className='text-3xl font-bold'>Your cart is empty</h2>
            </div>
              )}
        </>
  );
};

export default FormCheckout;
