'use client';
import React, { useEffect, useState } from 'react';
import { createProduct, getCategories } from '@/services/product/prouduct.service';
import { Button } from '@/components/ui/button';
import UploadDrag from '@/components/upload-drag/upload-drag';
import { CategoryType } from '@/types/product/product';
import Spinner from '@/components/spinner';
import { toast } from '@/components/ui/use-toast';
import { useRouter } from 'next/navigation';

const FormCreateProduct = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [categories, setCategories] = useState<CategoryType[]>([]);
  const [name, setName] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [price, setPrice] = useState<string>('');
  const [stock, setStock] = useState<string>('');
  const [category, setCategory] = useState<string>('');
  const [imgUrl, setImgUrl] = useState<string>('');
  const formData = new FormData();
  const navigate = useRouter();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoading(true);
    formData.append('name', name);
    formData.append('description', description);
    formData.append('price', price);
    formData.append('amountStock', stock);
    formData.append('idCategory', category);
    formData.append('imgUrl', imgUrl);
    const res = await createProduct(formData);
    setLoading(false);
    if (res) {
      toast({
        title: 'Product created successfully',
        description: 'We have sent you an email with the details of your product.'
      });
      setName('');
      setDescription('');
      setPrice('');
      setStock('');
      setCategory('');
      setImgUrl('');
      navigate.push('/admin/dashboard/products');
    }
  };
  useEffect(() => {
    const fetchCategories = async () => {
      const res = await getCategories();
      setCategories(res.raw);
    };
    fetchCategories();
  }, []);

  return (
    <div className='flex gap-16 w-full max-w-4xl'>
      <form onSubmit={handleSubmit} className='w-full'>
        <div className='w-full flex flex-col gap-4'>
          <div className='mb-4 w-full'>
            <label className='block mb-2 text-sm' htmlFor="name">
              Name
            </label>
            <input value={name} onChange={(e) => setName(e.target.value)} name='name' id='name' className='border w-full h-8 text-sm px-2 rounded-sm' placeholder='Name' type="text" />
          </div>
          <div className='mb-4 w-full' >
            <label className='block mb-2 text-sm' htmlFor="description">
              Description
            </label>
            <textarea value={description} onChange={(e) => setDescription(e.target.value)} name='description' id='description' className='border w-full h-20 text-sm px-2 rounded-sm resize-none' placeholder='Description' />
          </div>
          <div className='mb-4 w-full ' >
            <label className='block mb-2 text-sm' htmlFor="price">
              Price
            </label>
            <input value={price} onChange={(e) => setPrice(e.target.value)} name='price' id='price' className='border w-full h-8 text-sm px-2 rounded-sm' placeholder='Price' type="number" />
          </div>
          <div className='mb-4 w-full' >
            <label className='block mb-2 text-sm' htmlFor="quantity">
              Quantity
            </label>
            <input value={stock} onChange={(e) => setStock(e.target.value)} name='quantity' id='quantity' className='border w-full h-8 text-sm px-2 rounded-sm' placeholder='Quantity' type="number" />
          </div>
          <div className='mb-4 w-full' >
            <label className='block mb-2 text-sm' htmlFor="">
              Category
            </label>
            <select value={category} onChange={(e) => setCategory(e.target.value)} className='border text-gray-400 w-full h-8 text-sm px-2 rounded-sm bg-transparent' name="category" id="">
              <option value="">Select a category</option>
              {
                categories.map((category) => (
                  <option className='text-gray-500' key={category.id} value={category.id}>{category.name}</option>
                ))
              }
            </select>
          </div>
          <div className='w-full'>
            <Button className='w-full' type="submit">
              {loading ? <Spinner size='xs'/> : 'Create'}
            </Button>

          </div>
        </div>
      </form>
      <UploadDrag imgURl={imgUrl} setImgUrl={setImgUrl} />
    </div>
  );
};

export default FormCreateProduct;
