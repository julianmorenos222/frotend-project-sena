'use client';
import Spinner from '@/components/spinner';
import { Button } from '@/components/ui/button';
import { toast } from '@/components/ui/use-toast';
import UploadDrag from '@/components/upload-drag/upload-drag';
import { updateProduct } from '@/services/product/prouduct.service';
import { ProductItemType } from '@/types/product/product';
import React, { useState } from 'react';

type Props = {
  product: ProductItemType;
}

const FormEditProduct: React.FC<Props> = ({ product: { id, name, description, price, amountStock, idCategory, imgUrl } }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [nameProduct, setNameProduct] = useState<string>(name);
  const [descriptionProduct, setDescriptionProduct] = useState<string>(description);
  const [priceProduct, setPriceProduct] = useState<string>(price);
  const [stock, setStock] = useState<string>(amountStock);
  const [category, setCategory] = useState<string>(idCategory);
  const [imgUrlProduct, setImgUrlProduct] = useState<string>(imgUrl);
  const formData = new FormData();
  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoading(true);
    formData.append('name', nameProduct);
    formData.append('description', descriptionProduct);
    formData.append('price', priceProduct);
    formData.append('amountStock', stock);
    formData.append('idCategory', category);
    formData.append('imgUrl', imgUrlProduct);
    const res = await updateProduct(id, formData);
    setLoading(false);
    if (res) {
      toast({
        title: 'Product updated successfully',
        description: 'We have sent you an email with the details of your product.'
      });
    } else {
      toast({
        title: 'Error updating product',
        description: 'Please try again later.'
      });
    }
  };

  return (
    <div className='flex gap-16 w-full max-w-4xl'>
      <form onSubmit={handleSubmit} className='w-full'>
      <div className='w-full flex flex-col gap-4'>
        <div className='mb-4 w-full'>
          <label className='block mb-2 text-sm' htmlFor="name">
            Name
          </label>
          <input value={nameProduct} onChange={(e) => setNameProduct(e.target.value)} name='name' id='name' className='border w-full h-8 text-sm px-2 rounded-sm' placeholder='Name' type="text" />
        </div>
        <div className='mb-4 w-full' >
          <label className='block mb-2 text-sm' htmlFor="description">
            Description
          </label>
          <textarea value={descriptionProduct} onChange={(e) => setDescriptionProduct(e.target.value)} name='description' id='description' className='border w-full h-20 text-sm px-2 rounded-sm resize-none' placeholder='Description' />
        </div>
        <div className='mb-4 w-full ' >
          <label className='block mb-2 text-sm' htmlFor="price">
            Price
          </label>
          <input value={priceProduct} onChange={(e) => setPriceProduct(e.target.value)} name='price' id='price' className='border w-full h-8 text-sm px-2 rounded-sm' placeholder='Price' type="number" />
        </div>
        <div className='mb-4 w-full' >
          <label className='block mb-2 text-sm' htmlFor="quantity">
            Quantity
          </label>
          <input value={stock} onChange={(e) => setStock(e.target.value)} name='quantity' id='quantity' className='border w-full h-8 text-sm px-2 rounded-sm' placeholder='Quantity' type="number" />
        </div>
        <div className='mb-4 w-full' >
          <label className='block mb-2 text-sm' htmlFor="">
            Category
          </label>
          <input value={category} onChange={(e) => setCategory(e.target.value)} name='category' id='category' className='border w-full h-8 text-sm px-2 rounded-sm' placeholder='Category' type="text" />
        </div>
        <div className='w-full'>
          <Button className='w-full' type="submit">
            { loading ? <Spinner size='xs'/> : 'Update' }
          </Button>
        </div>
      </div>
    </form>
      <UploadDrag imgURl={imgUrlProduct} setImgUrl={setImgUrlProduct} />
    </div>
  );
};

export default FormEditProduct;
