import { Carousel, CarouselContent, CarouselItem, CarouselNext, CarouselPrevious } from '@/components/ui/carousel';
import React from 'react';

const CarouselItemContent:any[] = [
  { text: 'PRODUCTOS DE LA MAS ALTA CALIDAD', url: '/assets/img/HOMEPAGE.png' },
  { text: 'DEL CAMPO A TU CASA', url: '/assets/img/HOMEPAGE1.png' },
  { text: 'PRODUCTOS FRESCOS', url: '/assets/img/HOMEPAGE2.png' }
];

const Hero = () => {
  return (
    <div className='w-full mx-auto'>
       <Carousel className="w-full ">
      <CarouselContent>
        {CarouselItemContent.map((url, index) => (
          <CarouselItem className='p-0' key={index}>
          <div className='flex items-center justify-center w-full h-[600px] bg-cover bg-center bg-no-repeat' style={{ backgroundImage: `url(${url.url})` }}>
            <h2 className='text-7xl text-white font-bold animate-fade-in-down'>{ url.text}</h2>
            </div>
          </CarouselItem>
        ))}
      </CarouselContent>
      <CarouselPrevious className='abosolute left-10 text-white border-white border-2' />
      <CarouselNext className='abosolute right-10 text-white border-white border-2' />
    </Carousel>
    </div>
  );
};

export default Hero;
