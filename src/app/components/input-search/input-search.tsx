'use client';
import React, { useState } from 'react';
import { Search } from 'lucide-react';
import { useRouter } from 'next/navigation';

const InputSearch = () => {
  const [open, setOpen] = useState(false);
  const [search, setSearch] = useState('');
  const navigate = useRouter();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    navigate.push(`/product/category/${search}`);
  };
  return (
    <div>
      {
        !open && <Search color='#fff' onClick={() => setOpen(true)} className='w-6 h-6 cursor-pointer hover:opacity-80' />
      }
      {
        open &&
        <form onSubmit={handleSubmit} >
          <input className='w-[250px] outline-none border px-2 py-2  bg-gray-100 border-slate-400 text-sm animate-open-search' placeholder='Buscar...' type="text" value={search} onBlur={() => setOpen(false)} onChange={(e) => setSearch(e.target.value)} />
        </form>
      }
    </div>
  );
};

export default InputSearch;
