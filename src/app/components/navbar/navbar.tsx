'use client';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { NavigationMenu, NavigationMenuContent, NavigationMenuItem, NavigationMenuList, NavigationMenuTrigger } from '@/components/ui/navigation-menu';
import { getCategories } from '@/services/product/prouduct.service';
import { CategoryType } from '@/types/product/product';

const routes = [
  {
    name: 'Inicio',
    path: '/'
  },
  {
    name: 'Sobre nosotros',
    path: '/about-us'
  },
  {
    name: 'Contacto',
    path: '/contact'
  }
];

const Navbar = () => {
  const [category, setCategory] = useState<CategoryType[]>([]);
  useEffect(() => {
    const fetchCategories = async () => {
      const res = await getCategories();
      setCategory(res.raw);
    };
    fetchCategories();
  }, []);
  return (
    <nav className='sticky top-0 left-0 max-w-[1600px] mx-auto py-4'>

      <ul className='flex gap-10 items-center justify-center'>

        {routes.map((route) => (
          <li key={route.path}>
            <Link className='text-white font-medium text-sm hover:opacity-80 transition-all duration-150' href={route.path}>{route.name}</Link>
          </li>
        ))}
        <NavigationMenu>
          <NavigationMenuList>
            <NavigationMenuItem className='w-full'>
              <NavigationMenuTrigger className='bg- text-white rounded-none  font-medium' >Productos</NavigationMenuTrigger>
              <NavigationMenuContent >
                <ul className='px-2 py-2 w-48'>

                  {
                    category && category.map((category) => (
                      <li className='my-1' key={category.id}>
                        <Link className='text-gray-700 text-xs hover:opacity-80 transition-all duration-150' href={`/product/category/${category.name}`}>{category.name}</Link>
                      </li>
                    ))
                  }
                </ul>
              </NavigationMenuContent>
            </NavigationMenuItem>
          </NavigationMenuList>
        </NavigationMenu>
      </ul>
    </nav>
  );
};

export default Navbar;
