import { Button } from '@/components/ui/button';
import { Mail } from 'lucide-react';
import React from 'react';

const NewLetter = () => {
  return (
        <div className='w-full mt-20'>
            <div className='flex gap-4 justify-around py-28 max-w-[1400px] mx-auto'>
                <div>
                    <p className='text-3xl font-semibold'>Suscribete a nuestro boletín </p>
                </div>
                <form className='h-10 flex'>
                    <input placeholder='Ingresa tu correo' className='w-full h-full px-4 border border-slate-400 text-sm' type="text" />
                    <Button>
                        <Mail />
                    </Button>
                </form>
            </div>
        </div>
  );
};

export default NewLetter;
