import React from 'react';

const ShippingSupport = () => {
  return (
    <div className='w-full py-4 '>
      <p className='text-sm text-gray-500 text-center'>Pedidos realizados antes de las 12:00 pm se despachan el mismo día,
después de las 12:00 pm, tu pedido se despachara el dia siguiente</p>

    </div>
  );
};

export default ShippingSupport;
