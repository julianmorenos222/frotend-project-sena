'use client';
import React, { useContext } from 'react';
import { Sheet, SheetContent, SheetHeader, SheetTitle, SheetTrigger } from '@/components/ui/sheet';
import { Minus, Plus, ShoppingBag, Trash } from 'lucide-react';
import { Button } from '@/components/ui/button';
import { ShoppingCartContext, ShoppingCartType } from '@/context/products/shopping-cart';
import Image from 'next/image';
import Link from 'next/link';

const ShoppingCart = () => {
  const { items, removeOneItem, removeAllItems, amountPriceTotal, changeAmount } = useContext<ShoppingCartType>(ShoppingCartContext);

  return (
        <Sheet>
            <SheetTrigger className='relative'>
                <ShoppingBag color='#fff' className='w-6 h-6 cursor-pointer hover:opacity-80' />
                <p className='text-sm absolute -top-1 -right-3 bg-orange-500 text-white font-medium h-5 w-5 rounded-full'>{items.length}</p>
            </SheetTrigger>
            <SheetContent>
                <SheetHeader>
                    <SheetTitle> Carrito de compras</SheetTitle>
                </SheetHeader>
                <div className='flex flex-col justify-between h-full'>
                    <div className='flex-1 overflow-y-auto'>
                        <div className='flex justify-end items-center'>
                            <p className='text-sm text-gray-500 relative left-0 cursor-pointer' onClick={() => removeAllItems()}> Eliminar todo</p>

                        </div>
                        {

                            items.length > 0
                              ? (
                                  items.map((item) => (
                                        <div className='flex justify-between items-center border-b py-4' key={item.id}>
                                            <div className='flex gap-2'>
                                                <Image width={100} height={100} src={item.imgUrl} alt="" />
                                                <div>
                                                    <p className='text-sm text-gray-500'>{item.name}</p>
                                                    <p className='text-sm text-red-500'>${item.price}</p>

                                                </div>
                                            </div>
                                            <div className='flex '>
                                                <div className='flex items-center border bg-white px-4 rounded-l-md'>{item.amount}</div>
                                                <div className='flex gap-1 flex-col'>
                                                    <Button className='px-2 h-8 rounded-r-md' onClick={() => changeAmount(item.id, '+')}>
                                                        <Plus className='w-4 h-4' color='#fff' />
                                                    </Button>
                                                    <Button variant={'secondary'} className='px-2 h-8 rounded-r-md' onClick={() => changeAmount(item.id, '-')}>
                                                        <Minus className='w-4 h-4' color='#fff' />
                                                    </Button>
                                                </div>
                                            </div>
                                            <Trash onClick={() => removeOneItem(item.id)} />
                                        </div>
                                  )))
                              : (
                                    <div>
                                        <p> No hay productos</p>
                                    </div>
                                )
                        }
                    </div>
                    <div className='border-t border-gray-200  py-6 sm:px-6' >
                        <div className='flex justify-between text-base font-medium text-gray-900'>
                            <p>Subtotal</p>
                            <p>{amountPriceTotal}</p>
                        </div>
                        <p className='mt-0.5 text-sm text-gray-500'> Gastos de envío e impuestos calculados en el momento de la compra. </p>

                        <div className='mt-6'>
                            <Link href={'/checkout'}><Button className='w-full'>Ir al checkout</Button></Link>
                        </div>
                        <div className='mt-6 text-center flex justify-center text-sm text-gray-500'>
                            <p>
                                OR <button className='text-red-500 hover:text-red-300 transition-all duration-200'> Continua comprando</button>
                            </p>

                        </div>
                    </div>
                </div>
            </SheetContent>
        </Sheet>
  );
};

export default ShoppingCart;
