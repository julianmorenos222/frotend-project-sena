'use client';
import React from 'react';
import Link from 'next/link';
import { signOut, useSession } from 'next-auth/react';
import { Avatar, AvatarFallback, AvatarImage } from '@/components/ui/avatar';
import { ConciergeBell, GanttChartSquare, LayoutDashboard, LogOut, Settings } from 'lucide-react';
import { Button } from '@/components/ui/button';

const DashboardRoutes = [
  {
    name: 'Dashboard',
    path: '/admin/dashboard',
    icon: <LayoutDashboard />
  },
  {
    name: 'Order',
    path: '/admin/dashboard/order',
    icon: <ConciergeBell />
  },
  {
    name: 'Products',
    path: '/admin/dashboard/products',
    icon: <GanttChartSquare />
  },
  {
    name: 'Customers',
    path: '/',
    icon: <GanttChartSquare />
  },
  {
    name: 'Settings',
    path: '/',
    icon: <Settings />
  }
];

const SidebarDashboard = () => {
  const session = useSession();
  return (
    <div className='w-[300px] min-h-screen relative top-0 bg-[#095F59]'>
      <div className='flex relative h-full flex-col  items-center py-20'>
        <div className='flex flex-col items-center gap-2'>
          <Avatar className='w-20 h-20' >
            <AvatarImage src={'/assets/img/user_default.svg'} />
            <AvatarFallback>{ session.data?.user.raw.name?.charAt(0)}</AvatarFallback>
          </Avatar>
          <h4 className='text-lg font-semibold text-white'>{ session.data?.user.raw.name}</h4>
        </div>
        <ul className='flex flex-col gap-8 w-1/2 items-center py-4 mt-10'>
          {
            DashboardRoutes.map((route) => (
              <Link className='w-full ' href={route.path} key={route.name}>
                <li className='flex w-full   gap-2 items-center text-gray-300 text-sm hover:text-orange-600 '>
                  {route.icon}
                  {route.name}
                </li></Link>
            ))
          }

        <Button onClick={() => signOut()} className='flex w-full   gap-2 items-center text-gray-300 text-sm' >
          <LogOut />
          Log out
        </Button>
        </ul>
      </div>
    </div>
  );
};

export default SidebarDashboard;
