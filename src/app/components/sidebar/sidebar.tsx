import { getCategories } from '@/services/product/prouduct.service';
import { CategoryType } from '@/types/product/product';
import Link from 'next/link';

const fetchCategories = async () => {
  const res = await getCategories();
  return res.raw;
};

const Sidebar = async ({ params }: { params: { id: string } }) => {
  const id = params?.id;
  const data = await fetchCategories();
  return (
    <div className='w-full h-full p-4 border-r'>
      <h3 className='font-semibold'>Categories</h3>
      <ul className='flex flex-col gap-4 w-full p-4'>
        <li>
          <Link className={ id === 'all' ? 'text-orange-600' : ''} href={'/product/category/all'}>Todos</Link>
        </li>
        { data && data.map((category:CategoryType) => (
          <>
            <li key={category.id}>
              <Link className={ id === String(category.name) ? 'text-orange-600' : ''} href={`/product/category/${category.name}`}>{category.name}</Link>
            </li>
          </>
        ))}

      </ul>
    </div>
  );
};

export default Sidebar;
