'use client';
import PaginationContainer from '@/components/pagination/pagination-container';
import { AlertDialog, AlertDialogCancel, AlertDialogContent, AlertDialogDescription, AlertDialogFooter, AlertDialogHeader, AlertDialogTitle, AlertDialogTrigger } from '@/components/ui/alert-dialog';
import {
  Table,
  TableBody,
  TableCell,
  TableCaption,
  TableHead,
  TableHeader,
  TableRow

} from '@/components/ui/table';
import { getAllOrders } from '@/services/order/order.service';
import { formatterDate } from '@/utils/formatterDate';
import { Eye } from 'lucide-react';
import Image from 'next/image';
import React, { useState } from 'react';

const TableOrders = () => {
  const [products, setProducts] = useState<any[]>([]);
  const [offset, setOffset] = useState(1);
  return (
    <>

      <Table>
        <TableCaption className='my-6'>A list of all orders.</TableCaption>
        <TableHeader>
          <TableRow>
            <TableHead className="w-[100px]">ID</TableHead>
            <TableHead>Cliente id</TableHead>
            <TableHead>Descripcion</TableHead>
            <TableHead className="text-center">Fecha</TableHead>
            <TableHead className="text-center">Estado</TableHead>
            <TableHead className="text-center">Detalle de orden</TableHead>

          </TableRow>
        </TableHeader>
        <TableBody>
          {
            products && products.length > 0 && products.map((product) => (
              <TableRow key={product.id}>
                <TableCell className="font-medium">{product.id}</TableCell>
                <TableCell>{product.idClient}</TableCell>
                <TableCell>{product.description}</TableCell>
                <TableCell className="text-center">{formatterDate(product.dateOrder)}</TableCell>
                <TableCell className="text-center">{product.status}</TableCell>
                <TableCell className="text-center">
                  <AlertDialog >
                    <AlertDialogTrigger>
                      <Eye className='mx-auto' color='#0009' />
                    </AlertDialogTrigger>
                    <AlertDialogContent >
                      <AlertDialogHeader>
                        <AlertDialogTitle>Order details</AlertDialogTitle>
                      </AlertDialogHeader>
                      <AlertDialogDescription >
                        <div className='flex gap-4'>
                          {
                            product.detailOrder.map((detail: any) => (
                              <div key={detail.product.id}>
                                <Image src={detail.product.imgUrl} alt={detail.product.name} width={100} height={100} />
                                <p>Name: {detail.product.name}</p>
                                <p>Price: {detail.product.price}</p>
                                <p>Amount: {detail.amount}</p>
                              </div>))
                          }
                        </div>
                      </AlertDialogDescription>
                      <AlertDialogFooter>
                        <AlertDialogCancel >Exit</AlertDialogCancel>

                      </AlertDialogFooter>
                    </AlertDialogContent>
                  </AlertDialog>
                </TableCell>

              </TableRow>
            ))
          }
        </TableBody>
      </Table>
      <PaginationContainer
        getFunction={getAllOrders}
        setProducts={setProducts}
        products={products}
        setOffset={setOffset}
        offset={offset}
        numberButtons={3} />

    </>
  );
};

export default TableOrders;
