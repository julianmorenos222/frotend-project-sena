'use client';
import ModalConfirm from '@/components/modal-confirm/modal-confirm';
import PaginationContainer from '@/components/pagination/pagination-container';
import {
  Table,
  TableBody,
  TableCell,
  TableCaption,
  TableHead,
  TableHeader,
  TableRow

} from '@/components/ui/table';
import { deleteProduct, getAllProducts } from '@/services/product/prouduct.service';
import { ProductItemType } from '@/types/product/product';
import { AlertDialogTrigger } from '@radix-ui/react-alert-dialog';
import { Edit, Trash } from 'lucide-react';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';

const TableProducts = () => {
  const [products, setProducts] = useState<ProductItemType[]>([]);
  const [offset, setOffset] = useState(1);

  const handleDeleteProduct = async (id: string) => {
    const res = await deleteProduct(id);

    if (res) {
      const newProducts = products.filter((product) => product.id !== id);
      setProducts(newProducts);
    }
  };
  return (
    <>

      <Table>
        <TableCaption className='my-6'>A list of all products.</TableCaption>
        <TableHeader>
          <TableRow>
            <TableHead className="w-[100px]">ID</TableHead>
            <TableHead>Name</TableHead>
            <TableHead>Description</TableHead>
            <TableHead className="text-center">Price</TableHead>
            <TableHead className="text-center">Stock</TableHead>
            <TableHead className="text-center">Category</TableHead>
            <TableHead className="text-center">Image</TableHead>
            <TableHead className="text-center">Action</TableHead>

          </TableRow>
        </TableHeader>
        <TableBody>
          {
            products && products.length > 0 && products.map((product) => (
              <TableRow key={product.id}>
                <TableCell className="font-medium">{product.id}</TableCell>
                <TableCell>{product.name}</TableCell>
                <TableCell>{product.description}</TableCell>
                <TableCell className="text-center">{product.price}</TableCell>
                <TableCell className="text-center">{product.amountStock}</TableCell>
                <TableCell className="text-center">{product.idCategory}</TableCell>
                <TableCell className="text-center">
                  <Image className='mx-auto' src={product.imgUrl} alt={product.name} width={100} height={100} />
                </TableCell>
                <TableCell className='flex justify-center gap-4'>
                  <Link href={`/admin/dashboard/products/edit-product/${product.id}`}>
                    <Edit />
                  </Link>
                  {/*  modal confirm */}
                  <ModalConfirm handleConfirmAction={() => handleDeleteProduct(product.id)} trigger={
                    <AlertDialogTrigger>
                      <Trash className='hover:text-red-500' />
                    </AlertDialogTrigger>
                  } />
                </TableCell>
              </TableRow>
            ))
          }
        </TableBody>
      </Table>
          <PaginationContainer
            getFunction={getAllProducts}
            setProducts={setProducts}
            products={products}
            setOffset={setOffset}
            offset={offset}
            numberButtons={3} />

    </>
  );
};

export default TableProducts;
