import Footer from '@/components/footer';
import Header from '@/components/header/header';
import { Mail, Map, Phone } from 'lucide-react';
import React from 'react';

const page = () => {
  return (
    <>
      <Header />
      <div className='w-full flex flex-col items-center justify-center min-h-screen gap-16'>
        <h1 className='text-3xl font-bold text-primary_green'>Contactate con Agrosmart</h1>
        <div className='flex gap-16 border-2 p-10 border-primary_green'>
          <div className='flex flex-col gap-4'>
            <div className='flex items-center gap-2'>
            <Map />
            <h3 className='text-lg font-medium text-primary_green'>Dirección</h3>

            </div>
            <p >Calima - Darien,Valle del cauca, Colombia</p>

          </div>
          <div className='flex flex-col gap-4'>
            <div className='flex items-center gap-2'>
              <Phone />
              <h3 className='text-lg font-medium text-primary_green'>Telefonos</h3>
            </div>
            <p>+57 123 456 7890</p>
            <p>+57 987 654 3210</p>
          </div>
          <div className='flex flex-col gap-4'>
            <div className='flex items-center gap-2'>
            <Mail />
              <h3 className='text-lg font-medium text-primary_green'>Email</h3>
            </div>
            <p>6HnZt@example.com</p>

          </div>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d790.6802542845965!2d-76.4828329554792!3d3.9296122452059596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sco!4v1709846642919!5m2!1ses-419!2sco" width="900" height="450" style={{ border: 0 }} loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
      </div>
      <Footer />
    </>
  );
};

export default page;
