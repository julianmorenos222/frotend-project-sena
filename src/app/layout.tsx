import type { Metadata } from 'next';
import { Poppins } from 'next/font/google';
import './styles/globals.css';
import React from 'react';
import SessionAuthProvider from '@/context/session-auth-provider/session-auth-provider';
import { Toaster } from '@/components/ui/toaster';
import '@uploadthing/react/styles.css';
import { ShoppingCartProvider } from '@/context/products/shopping-cart';

const poppins = Poppins({ subsets: ['latin'], weight: ['100', '200', '300', '400', '500', '600', '700'] });

export const metadata: Metadata = {
  title: 'AgroSmart',
  description: 'AgroSmart is a e-commerce platform.',
  icons: '/assets/img/Logo2.svg'
};

export default function RootLayout ({
  children
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`${poppins.className}`}>
        <SessionAuthProvider>
          <ShoppingCartProvider>
            {children}
          </ShoppingCartProvider>
        </SessionAuthProvider>
        <Toaster />
      </body>
    </html>
  );
}
