import AllProducts from './components/all-products';
import Footer from '../components/footer';
import Hero from './components/hero';
import Header from '@/components/header';
import NewLetter from './components/new-letter';
import ShippingSupport from './components/shipping-support';

export default function Home () {
  return (
    <>
      <Header />
      <main className='' >
        <ShippingSupport/>
        <Hero />
        <div className='pt-10'>
          <AllProducts />
          <NewLetter />
        </div>
      </ main>
      <Footer />
    </>
  );
}
