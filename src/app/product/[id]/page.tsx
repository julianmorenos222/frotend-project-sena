import DetailProduct from '@/app/components/detail-product/detail-product';
import { getOneProduct } from '@/services/product/prouduct.service';
import React from 'react';

const page = async ({ params }: { params: { id: string } }) => {
  const res = await getOneProduct(Number(params.id));
  return (
    <div className="w-full py-20">
        {res && res.raw && <DetailProduct product={res.raw} />}
    </div>
  );
};

export default page;
