import CardProduct from '@/app/components/card-product';
import Sidebar from '@/app/components/sidebar';
import { searchProduct } from '@/services/product/prouduct.service';
import { ProductItemType } from '@/types/product/product';
import React from 'react';

const page = async ({ params }: { params: { id: string } }) => {
  const id = params.id;
  const res = await searchProduct({ c: id, s: id });
  return (
    <div className="w-full py-20">
      <div className="grid grid-cols-6 gap-4 w-full max-w-[1600px] mx-auto ">
        <div className=''>
          <Sidebar params={params} />
        </div>
        <div className="grid col-span-5 min-h-96 justify-end grid-cols-4 gap-10">
          {
            res && res.raw &&
            res.raw.map((product:ProductItemType, index:number) => (
              <CardProduct product={product} key={index} />
            ))
          }
          {
            res && res.raw && res.raw.length === 0 && <p className="text-center text-xl col-span-4">No hay productos</p>
          }
        </div>
      </div>
    </div>
  );
};

export default page;
