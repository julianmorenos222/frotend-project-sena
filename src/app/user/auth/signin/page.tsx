import FormSignin from '@/components/form-signin';
import React from 'react';

const SignIn = () => {
  return (
    <div className='w-full flex items-center justify-center min-h-screen'>
      <div>
        <h1 className='text-3xl font-bold mb-8 text-center'>Sign In</h1>
        <FormSignin />
      </div>
    </div>
  );
};

export default SignIn;
