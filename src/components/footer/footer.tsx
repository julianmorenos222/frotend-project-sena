import Link from 'next/link';
import React from 'react';

const Footer = () => {
  return (
        <footer>
            <div className='bg-secondary_green mx-auto pt-20 pb-5'>
                <section className='flex text-sm text-destructive max-w-[1400px] mx-auto'>
                    <div className='flex-1 space-y-4'>
                        <article>
                            <h2 className='text-3xl font-semibold text-white'>AgroSmart</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                        </article>
                        <article>
                            <p className='text-white text-lg'>Calima, Valle del cauca Colombia </p>
                            <p className='text-white text-lg'>+57 987 654 3210</p>
                            <p className='text-white text-lg'>nVgJt@example.com</p>

                        </article>
                    </div>
                    <div className='flex flex-1 justify-between px-6 gap-4'>
                        <article>
                            <h3 className='font-semibold text-white'>Corporativo</h3>
                            <ul className='flex flex-col gap-2 mt-2'>
                                <Link href={'#'}><li>Sobre Nosotros</li></Link>
                                <Link href={'#'}><li>Contacto</li></Link>
                                <Link href={'#'}><li>Blog</li></Link>
                            </ul>
                        </article>
                        <article>
                            <h3 className='font-semibold text-white'>Enlaces de Interés</h3>
                            <ul className='flex flex-col gap-2 mt-2'>
                                <Link href={'#'} ><li>Politicas de privacidad</li></Link>
                                <Link href={'#'} ><li>Detalles de envíos</li></Link>
                                <Link href={'#'} ><li>Terminos y Condiciones</li></Link>
                            </ul>
                        </article>
                    </div>
                </section>

            </div>
            <div className='py-4 bg-[#0C766F]'>
                <p className='text-destructive max-w-7xl mx-auto font-light text-sm text-center'>AgroSmart © – All rights reserved</p>
            </div>
        </footer>
  );
};

export default Footer;
