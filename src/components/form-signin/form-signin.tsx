'use client';
import React, { useState } from 'react';
import { Button } from '../ui/button';
import { login } from '@/services/auth/auth.service';
import { useRouter } from 'next/navigation';
import { useToast } from '@/components/ui/use-toast';
import Spinner from '../spinner';

const FormSignin = () => {
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const router = useRouter();
  const { toast } = useToast();

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoading(true);
    const res: any = await login(username, password);

    if (res.ok) {
      router.push('/admin/dashboard');
      toast({
        title: 'Sign in',
        description: 'You have successfully signed in.'

      });
    } else {
      toast({
        title: 'Sign in',
        description: 'Something went wrong.'
      });
    }

    setLoading(false);
  };
  return (
    <div className='min-w-[300px] w-[450px] mx-auto bg-white py-8 px-4'>
      <form onSubmit={onSubmit} className='flex flex-col gap-8' >
        <div>
          <label htmlFor="username">
          </label>
          <div className=' h-8'>
            <input onChange={(e) => setUsername(e.target.value)} className='border-b-2 w-full h-full text-sm outline-none px-2 py-4 bg-transparent focus:border-b-slate-400 transition-all duration-700' id="username" name="username" placeholder="Username" type="text" />
          </div>
        </div>
        <div>
          <label htmlFor="password">
          </label>
          <div className=' h-8'>
            <input onChange={(e) => setPassword(e.target.value)} className='border-b-2 w-full h-full  text-sm outline-none px-2 py-4 bg-transparent focus:border-b-slate-400 transition-all duration-700' id="password" name="password" placeholder="Password" type="password" />
          </div>
        </div>
        <Button type='submit' className='rounded-md w-full'>
          {loading ? <Spinner size='xs'/> : 'Sign in'}
        </Button>
      </form>
    </div>
  );
};

export default FormSignin;
