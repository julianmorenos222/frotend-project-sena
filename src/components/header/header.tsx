import InputSearch from '@/app/components/input-search';
import Navbar from '@/app/components/navbar';
import ShoppingCart from '@/app/components/shopping-cart';
import Logo from '@/components/logo';
import Link from 'next/link';
import React from 'react';

const Header = () => {
  return (
    <>
      <header className='w-full sticky top-0 bg-primary_green pt-3 z-40'>

        <div className='flex w-full mx-auto justify-between items-center max-w-[1600px]'>
          <div className='flex items-center'>
            <Link href={'/'}>
              <Logo value={'/assets/img/Logo.svg'} height={50} width={50} />

            </Link>
            <p className='text-xl font-semibold text-white'>Agro<span className='text-orange'>Smart</span></p>
          </div>
          <Navbar />
          <div className='flex gap-4'>
            <InputSearch />
            <ShoppingCart />

          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
