import Image from 'next/image';
import React from 'react';

type Props = {
    value: string,
    height?: number,
    width?: number
};

const Logo: React.FC<Props> = ({ value, height, width }) => {
  return (
    <div>
        <Image src={value} alt="logo" width={width} height={height}/>
    </div>
  );
};

export default Logo;
