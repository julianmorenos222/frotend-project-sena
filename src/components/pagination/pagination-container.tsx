'use client';
import React, { useEffect, useState } from 'react';
import { Pagination, PaginationContent, PaginationItem, PaginationLink, PaginationNext, PaginationPrevious } from '@/components/ui/pagination';
import { ProductItemType, ResponseAPIProduct } from '@/types/product/product';

type Props = {
    getFunction: ({ offset, limit }: { offset: string; limit: string }) => Promise<ResponseAPIProduct>;
    products: ProductItemType[];
    setProducts: React.Dispatch<React.SetStateAction<ProductItemType[]>>;
    setOffset: React.Dispatch<React.SetStateAction<number>>;
    offset: number;
    numberButtons: number
}
const PaginationContainer: React.FC<Props> = ({ products, setOffset, offset, numberButtons, setProducts, getFunction }) => {
  const [array, setArray] = useState(Array.from({ length: numberButtons }, (_, index) => index + 1));

  const changePage = () => {
    if (offset > array[array.length - 1]) {
      const newArray = array.map((item, index) => (
        array[array.length - 1] + (index + 1)
      ));
      setArray(newArray);
    }
    if (offset < array[0]) {
      const newArray = array.map((item, index) => (
        array[array.length - 1] - (index + 1)
      ));
      setArray(newArray);
    }
  };
  useEffect(() => {
    const getProducts = async () => {
      const res = await getFunction({ offset: String(offset), limit: '6' });
      console.log(res.raw);
      setProducts(res.raw);
    };
    changePage();
    getProducts();
  }, [offset, array]);

  return (
       <>
       {
        products && (
            <Pagination>
            <PaginationContent>
                {
                    offset > 1 && (
                        <PaginationItem >
                            <PaginationPrevious className='text-white' onClick={() => setOffset(offset - 1)} href="#" />
                        </PaginationItem>
                    )
                }
                {
                    array.map((number) => (
                        <PaginationItem key={number}>
                            <PaginationLink key={number} className={ offset === number ? '' : 'text-white'} isActive={offset === number} href="#" onClick={() => setOffset(number)}>
                                {number}
                            </PaginationLink>
                        </PaginationItem>
                    ))
                }
                {
                    products.length > 0 && (
                        <PaginationItem >
                            <PaginationNext className='text-white' onClick={() => setOffset(offset + 1)} href="#" />
                        </PaginationItem>
                    )
                }
            </PaginationContent>
        </Pagination>
        )
       }
       </>
  );
};

export default PaginationContainer;
