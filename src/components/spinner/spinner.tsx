import React from 'react';

import { cn } from '@/lib/utils';
import { cva } from 'class-variance-authority';

interface SpinnerProps {
  className?: string;
  size?: 'default' | 'xs' | 'sm' | 'lg';
}

const spinnerVariants = cva(
  'inline-block animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]',
  {
    variants: {
      size: {
        default: 'h-6 w-6',
        xs: 'h-4 w-4',
        sm: 'h-7 w-7',
        lg: 'h-8 w-8'
      }
    }
  }
);

const Spinner: React.FC<SpinnerProps> = ({ className, size }) => {
  return (
    <div className={cn(spinnerVariants({ size }), className)} role="status">
      <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
        Loading...
      </span>
    </div>
  );
};

export default Spinner;
