'use client';
import { UploadDropzone } from '@/utils/uploadthing';
import Image from 'next/image';
import React from 'react';

type Props={
  imgURl:string
  setImgUrl:(url:string)=>void
}
const UploadDrag:React.FC<Props> = ({ imgURl, setImgUrl }) => {
  return (
    <div className='flex items-center flex-col gap-16'>
      <UploadDropzone
        endpoint='imageUploader'
        onClientUploadComplete={(res) => {
          setImgUrl(res[0].url);
        }}
        onUploadError={(error: Error) => {
          throw error;
        }}
        />
        { imgURl.length ? <Image width={200} height={500} alt={'image'} src={imgURl} /> : null }

    </div>
  );
};

export default UploadDrag;
