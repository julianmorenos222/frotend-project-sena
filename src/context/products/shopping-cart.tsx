'use client';
import { toast } from '@/components/ui/use-toast';
import { ProductItemType } from '@/types/product/product';
import { convertToPrice } from '@/utils/converterToPrice';
import { FC, ReactNode, createContext, useState } from 'react';

interface ProductItemTypeWithAmount extends ProductItemType {
  amount: number
}
export type ShoppingCartType = {
  items: ProductItemTypeWithAmount[]
  addItem: (product: ProductItemType) => void
  removeOneItem: (id: string) => void
  removeAllItems: () => void,
  amountPriceTotal: number | string,
  changeAmount: (productId: string, operation: '+' | '-') => void
}
export const ShoppingCartContext = createContext<ShoppingCartType>({
  items: [],
  addItem: () => { },
  removeOneItem: () => { },
  removeAllItems: () => { },
  amountPriceTotal: 0,
  changeAmount: () => { }

});
type Props = {
  children: ReactNode
}
export const ShoppingCartProvider: FC<Props> = ({ children }) => {
  const [items, setItems] = useState<ProductItemTypeWithAmount[]>([]);
  const addItem = (product: ProductItemType) => {
    if (items.some((item) => item.id === product.id)) {
      return toast({
        title: '',
        description: 'Already in cart'
      });
    }
    setItems([...items, { ...product, amount: 1 }]);
  };
  const removeOneItem = (id: any) => setItems(items.filter((item) => item.id !== id));
  const removeAllItems = () => setItems([]);
  const priceTotal = items.reduce((total, item) => (total + Number(item.price)) * item.amount, 0);
  const converterAmount = convertToPrice(priceTotal);
  const changeAmount = (productId: string, operation: '+' | '-') => {
    const newItems = items.map((item) => {
      if (item.id === productId) {
        if (operation === '+') {
          if (item.amount === 5) return item;
          return { ...item, amount: item.amount + 1 };
        } else {
          if (item.amount === 1) return item;
          return { ...item, amount: item.amount - 1 };
        }
      } else {
        return item;
      }
    });
    setItems(newItems);
  };
  const data: ShoppingCartType = {
    items,
    addItem,
    removeOneItem,
    removeAllItems,
    amountPriceTotal: converterAmount,
    changeAmount
  };
  return (
    <ShoppingCartContext.Provider value={data}>{children}</ShoppingCartContext.Provider>
  );
};
