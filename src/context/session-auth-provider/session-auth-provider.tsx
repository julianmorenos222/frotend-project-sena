'use client';
import { SessionProvider } from 'next-auth/react';
import React from 'react';

interface SessionAuthProviderProps {
  children: React.ReactNode;
}

const SessionAuthProvider: React.FC<SessionAuthProviderProps> = ({
  children
}) => {
  return <SessionProvider>{children}</SessionProvider>;
};

export default SessionAuthProvider;
