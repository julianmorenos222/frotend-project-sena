import { signIn } from 'next-auth/react';

/* login service */

const login = async (username: string, password: string) => {
  try {
    const responseNextAuth = await signIn('credentials', {
      username,
      password,
      redirect: false
    });
    return responseNextAuth;
  } catch (error) {
    return error;
  }
};

export { login };
