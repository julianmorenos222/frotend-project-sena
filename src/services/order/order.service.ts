import { OrderType } from '@/types/order/order';
import { session } from '@/utils/getSession';

type OrderTypeWithOutId = Omit<OrderType, 'id'>

export const createOrder = async (data: OrderTypeWithOutId) => {
  const token = await session();
  const res = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}order`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: token as string

    },
    body: JSON.stringify(data)
  });
  return res.json();
};

export const getAllOrders = async (query?: { offset?: string; limit?: string }) => {
  const token = await session();
  return fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}order?${query?.offset ? `offset=${query.offset}` : ''}${query?.limit ? `&limit=${query.limit}` : ''} `, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: token as string
    }
  }).then(res => res.json());
};
