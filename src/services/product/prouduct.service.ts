import { ResponseAPIProduct } from '@/types/product/product';
import { session } from '@/utils/getSession';

const createProduct = async (data: any) => {
  const token = await session();
  const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}products/`, {
    method: 'POST',
    body: data,
    headers: { Authorization: token as string }
  });
  return response.json();
};

const getAllProducts = async (query?: { offset?: string; limit?: string }): Promise<ResponseAPIProduct> => {
  const token = await session();
  const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}products?${query?.offset ? `offset=${query.offset}` : ''}${query?.limit ? `&limit=${query.limit}` : ''} `, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: token as string
    }
  });
  return response.json();
};

const getAllProductsByCategory = async (id: string) => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}products/category/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  return response.json();
};

const deleteProduct = async (id: string) => {
  const token = await session();
  const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}products/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Authorization: token as string
    }
  });
  return response.json();
};

const updateProduct = async (id: string, data: any) => {
  const token = await session();
  const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}products/${id}`, {
    method: 'PUT',
    body: data,
    headers: {
      Authorization: token as string
    }
  });
  return response.json();
};

const getOneProduct = async (id: number) => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}products/one/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  return response.json();
};

const getCategories = async () => {
  const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}products/categories`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  return response.json();
};

const searchProduct = async (query: { s: string; c: string }) => {
  const { s, c } = query;
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}products/search/?s=${s || NaN}&c=${c || NaN}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    if (response.status === 204) return { raw: [] };
    return response.json();
  } catch (error) {
    return error;
  }
};
export { createProduct, getAllProducts, deleteProduct, getOneProduct, updateProduct, getCategories, getAllProductsByCategory, searchProduct };
