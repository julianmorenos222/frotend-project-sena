export type DetailOrderType = {
    amount: number
    unitPrice: number
    idProduct: number
}

export type OrderType = {
    id: string
    name: string
    lastName: string
    phone: string
    email: string
    description: string
    address: string
    city: string
    zipCode: string
    detailOrder: DetailOrderType[]
}
