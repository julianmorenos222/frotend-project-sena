export type ProductItemType = {
    id: string
    name: string
    description: string
    price: string
    amountStock: string
    imgUrl: string
    idCategory: string
    date: string
}

export type CategoryType = {
    id:string
    name: string
}
export type ResponseAPIProduct = {
    raw: ProductItemType[]
}
