export const convertToPrice = (amount: number, moneda = 'COP') => {
  const formatter = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: moneda
  });
  return formatter.format(amount);
};
