import { getSession } from 'next-auth/react';

export const session = async () => {
  const session = await getSession();
  if (session && session.user && session.user.raw.token) {
    return `Bearer ${session?.user.raw.token}`;
  }
};
