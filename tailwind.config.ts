import type { Config } from 'tailwindcss';
import { withUt } from 'uploadthing/tw';

export const widths = withUt({
  // Your existing Tailwind config
  content: ['./src/**/*.{ts,tsx,mdx}']

});

const config = {
  darkMode: ['class'],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}'
  ],
  prefix: '',
  theme: {
    container: {
      center: true,
      padding: '2rem',
      screens: {
        '2xl': '1400px'
      }
    },
    extend: {
      colors: {
        border: 'hsl(var(--border))',
        input: 'hsl(var(--input))',
        ring: 'hsl(var(--ring))',
        background: 'hsl(var(--background))',
        foreground: 'hsl(var(--foreground))',
        primary: {
          DEFAULT: '#7AC750',
          foreground: 'hsl(var(--primary-foreground))'
        },
        secondary: {
          DEFAULT: '#222',
          foreground: 'hsl(var(--secondary-foreground))'
        },
        destructive: {
          DEFAULT: '#ECECEC',
          foreground: 'hsl(var(--destructive-foreground))'
        },
        primary_green: {
          DEFAULT: '#095F59',
          foreground: 'hsl(var(--destructive-foreground))'
        },
        secondary_green: {
          DEFAULT: '#0C766F',
          foreground: 'hsl(var(--destructive-foreground))'
        },
        pastel_primary: {
          DEFAULT: '#FFE3A8',
          foreground: 'hsl(var(--destructive-foreground))'
        },
        pastel_secondary: {
          DEFAULT: '#FFFCF1',
          foreground: 'hsl(var(--destructive-foreground))'
        },
        orange: {
          DEFAULT: '#FDCF6E',
          foreground: 'hsl(var(--destructive-foreground))'
        },
        muted: {
          DEFAULT: '#A86A3D',
          foreground: 'hsl(var(--muted-foreground))'
        },
        popover: {
          DEFAULT: 'hsl(var(--popover))',
          foreground: 'hsl(var(--popover-foreground))'
        },
        card: {
          DEFAULT: 'hsl(var(--card))',
          foreground: 'hsl(var(--card-foreground))'
        }
      },
      borderRadius: {
        lg: 'var(--radius)',
        md: 'calc(var(--radius) - 2px)',
        sm: 'calc(var(--radius) - 4px)'
      },
      keyframes: {
        'accordion-down': {
          from: { height: '0' },
          to: { height: 'var(--radix-accordion-content-height)' }
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: '0' }
        },
        'fade-in-down': {
          '0%': {
            opacity: '0',
            transform: 'translateY(100%)'
          },
          '100%': {
            opacity: '1',
            transform: 'translateY(0)'
          }
        },
        'open-search': {
          '0%': { width: '0' },
          '100%': { width: '250px' }
        }
      },
      animation: {
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
        'fade-in-down': 'fade-in-down 1s ease-out',
        'open-search': 'open-search 0.2s ease-out'
      }
    }
  },
  plugins: [require('tailwindcss-animate')]
} satisfies Config;

export default config;
